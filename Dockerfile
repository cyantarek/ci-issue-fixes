FROM alpine

#Install CA Certificatio
#Required for facebook callback varification
RUN apk add --no-cache ca-certificates

WORKDIR /go/src/gitlab.com/cyantarek/ci-issue-fixes/gateway
COPY build/gateway /go/src/gitlab.com/cyantarek/ci-issue-fixes/gateway/

# RUN apk add --update --no-cache ca-certificates

ENV GOPATH /go

ENTRYPOINT /go/src/gitlab.com/shophobe-engine/gateway/gateway run

# Service listens on port 80.
EXPOSE 80
EXPOSE 443